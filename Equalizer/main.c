#include "sound.h"

#include <complex.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#define SAMPLE_SIZE     4096
#define SAMPLE_PROC     SAMPLE_SIZE/2
#define TRACKSIZE       sizeof(sound_table)


const double pi=3.14159265358979;
enum Filter{Low_Pass/*,Mid_Pass,High_Pass*/};

int16_t samples[SAMPLE_SIZE];
complex processing[SAMPLE_PROC];

//function definition below the main
void DFT(complex*,unsigned);
void iDFT(complex *data, unsigned N);
void filter(int N, complex*,enum Filter);

int main(void){
    int controll=0;
    int max=TRACKSIZE/SAMPLE_PROC;
    double sum1=0,sum2=0;
    int counter=0;
    int tablepos=0;
    double tmp=0;
    double tmp2=0;
    double great=0;
    //"Endless" loop
    while(true && controll<max){
        //copy Soundtable to processing
        counter=0;
        while(counter<SAMPLE_PROC){
            tmp=(((uint8_t)sound_table[tablepos]-0x80));
            tmp/=SAMPLE_SIZE;
            processing[counter]=tmp;
            tablepos++;
            counter++;
            if(tablepos>=TRACKSIZE) tablepos=0;

        }
        //amplitude to frequencies
        DFT(processing,SAMPLE_PROC);
        //Ausgabe
        counter=0;
        while(counter<SAMPLE_PROC){
            tmp2=cabs(processing[counter++]);
            if(great<abs(tmp2))
                great=abs(tmp2);
            printf("%f\t%f\n",tmp2,great);
        }
        counter=0;
        //Filter
        //filter(SAMPLE_PROC,processing,Low_Pass);
        //frequencies to amplitude
        //iDFT(processing,SAMPLE_PROC);


        //getc(stdin);
        controll++;
    }


    return EXIT_SUCCESS;

}


void DFT(complex *data, unsigned N)
{
    bool error=false; // error flag
    complex *tmp=NULL; // pointer to temporary data
    int k, n; // local index variables
    complex wActual; // actual rotation factor
    complex wStep; // step between rotation factors
    // get memory for temporary data
    error = error || (tmp=malloc(sizeof(complex)*N))==NULL;
    // initialise temporary data
    for(n=0; !error && n<N; n++) tmp[n] = 0+0*I;
    // loop over all target values
    for(n=0; !error && n<N; n++) {
        // actual angle and angle step
        wActual = 1+0*I;
        wStep = cos(-2*pi*n/N)+I*sin(-2*pi*n/N);
        // loop over all source values
        for(k=0; k<N; k++) {
            tmp[n] += data[k]*wActual;
            wActual *= wStep;
        }
    }
    // copy data
    for(n=0; !error && n<N; n++) data[n] = tmp[n];
    // free memory
    if(tmp) free(tmp);
}

void iDFT(complex *data, unsigned N)
{
    bool error=false; // error flag
    complex *tmp=NULL; // pointer to temporary data
    int k, n; // local index variables
    complex wActual; // actual rotation factor
    complex wStep; // step between rotation factors
    // get memory for temporary data
    error = error || (tmp=malloc(sizeof(complex)*N))==NULL;
    // initialise temporary data
    for(n=0; !error && n<N; n++) tmp[n] = 0+0*I;
    // loop over all target values
    for(n=0; !error && n<N; n++) {
        // actual angle and angle step
        wActual = 1+0*I;
        wStep = cos(-2*pi*n/N)+I*sin(-2*pi*n/N);
        // loop over all source values
        for(k=0; k<N; k++) {
            tmp[n] += data[k]*wActual;
            wActual *= wStep;
        }
    }
    // copy data
    for(n=0; !error && n<N; n++) data[n] = tmp[n];
    // free memory
    if(tmp) free(tmp);
}
void filter(int N,complex* data,enum Filter filter){
	int i;
	int count=1;
	if(filter==Low_Pass){
		for(i=N/3;i<N;i++){
			if(i<2*N/3)
				data[i]=data[i]/count++;
			else{
				data[i]=0;
			}
		}
	}
}
